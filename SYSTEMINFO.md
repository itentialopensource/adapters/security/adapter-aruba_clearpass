# Aruba Clearpass

Vendor: Aruba Networks
Homepage: https://www.arubanetworks.com/

Product: Clearpass
Product Page: https://www.arubanetworks.com/products/security/network-access-control/secure-access/

## Introduction
We classify Aruba Clearpass into the Security (SASE) domain as it delivers comprehensive network access control and policy management for secure and seamless connectivity across diverse devices and users.

## Why Integrate
The Aruba Clearpass adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Aruba Clearpass. With this adapter you have the ability to perform operations such as:

- Security Group updates
- Security Rule Removal
- Security Rule Creation
- Run network compliance checks and remediation
- Determine configuration drift
- Device Onboarding
- Software Upgrade
- Firewall Configuration backup or restore

## Additional Product Documentation
The [Aruba Clearpass REST API Reference](https://developer.arubanetworks.com/aruba-cppm/docs/clearpass-configuration)
