# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Aruba_clearpass System. The API that was used to build the adapter for Aruba_clearpass is usually available in the report directory of this adapter. The adapter utilizes the Aruba_clearpass API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Aruba Clearpass adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Aruba Clearpass. With this adapter you have the ability to perform operations such as:

- Security Group updates
- Security Rule Removal
- Security Rule Creation
- Run network compliance checks and remediation
- Determine configuration drift
- Device Onboarding
- Software Upgrade
- Firewall Configuration backup or restore

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
