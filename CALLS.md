## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Aruba Clearpass. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Aruba Clearpass.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Aruba_clearpass. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getForStaticHostList(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_StaticHostList</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForStaticHostList(body, callback)</td>
    <td style="padding:15px">POST_for_StaticHostList</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForStaticHostList(staticHostListId, callback)</td>
    <td style="padding:15px">GET_for_StaticHostList</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForStaticHostList(staticHostListId, body, callback)</td>
    <td style="padding:15px">PATCH_for_StaticHostList</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForStaticHostList(staticHostListId, body, callback)</td>
    <td style="padding:15px">PUT_for_StaticHostList</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForStaticHostList(staticHostListId, callback)</td>
    <td style="padding:15px">DELETE_for_StaticHostList</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForStaticHostListByName(name, callback)</td>
    <td style="padding:15px">GET_for_StaticHostList_by_name</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForStaticHostListByName(name, body, callback)</td>
    <td style="padding:15px">PATCH_for_StaticHostList_by_name</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForStaticHostListByName(name, body, callback)</td>
    <td style="padding:15px">PUT_for_StaticHostList_by_name</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForStaticHostListByName(name, callback)</td>
    <td style="padding:15px">DELETE_for_StaticHostList_by_name</td>
    <td style="padding:15px">{base_path}/{version}/static-host-list/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForRole(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_Role</td>
    <td style="padding:15px">{base_path}/{version}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForRole(body, callback)</td>
    <td style="padding:15px">POST_for_Role</td>
    <td style="padding:15px">{base_path}/{version}/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForRole(roleId, callback)</td>
    <td style="padding:15px">GET_for_Role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForRole(roleId, body, callback)</td>
    <td style="padding:15px">PATCH_for_Role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForRole(roleId, body, callback)</td>
    <td style="padding:15px">PUT_for_Role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForRole(roleId, callback)</td>
    <td style="padding:15px">DELETE_for_Role</td>
    <td style="padding:15px">{base_path}/{version}/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForRoleByName(name, callback)</td>
    <td style="padding:15px">GET_for_Role_by_name</td>
    <td style="padding:15px">{base_path}/{version}/role/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForRoleByName(name, body, callback)</td>
    <td style="padding:15px">PATCH_for_Role_by_name</td>
    <td style="padding:15px">{base_path}/{version}/role/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForRoleByName(name, body, callback)</td>
    <td style="padding:15px">PUT_for_Role_by_name</td>
    <td style="padding:15px">{base_path}/{version}/role/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForRoleByName(name, callback)</td>
    <td style="padding:15px">DELETE_for_Role_by_name</td>
    <td style="padding:15px">{base_path}/{version}/role/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForLocalUserPasswordPolicy(callback)</td>
    <td style="padding:15px">GET_for_LocalUserPasswordPolicy</td>
    <td style="padding:15px">{base_path}/{version}/local-user/password-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForLocalUserPasswordPolicy(body, callback)</td>
    <td style="padding:15px">PUT_for_LocalUserPasswordPolicy</td>
    <td style="padding:15px">{base_path}/{version}/local-user/password-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForLocalUserPasswordPolicy(body, callback)</td>
    <td style="padding:15px">PATCH_for_LocalUserPasswordPolicy</td>
    <td style="padding:15px">{base_path}/{version}/local-user/password-policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForLocalUser(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_LocalUser</td>
    <td style="padding:15px">{base_path}/{version}/local-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForLocalUser(body, callback)</td>
    <td style="padding:15px">POST_for_LocalUser</td>
    <td style="padding:15px">{base_path}/{version}/local-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForLocalUser(localUserId, callback)</td>
    <td style="padding:15px">GET_for_LocalUser</td>
    <td style="padding:15px">{base_path}/{version}/local-user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForLocalUser(localUserId, body, callback)</td>
    <td style="padding:15px">PATCH_for_LocalUser</td>
    <td style="padding:15px">{base_path}/{version}/local-user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForLocalUser(localUserId, body, callback)</td>
    <td style="padding:15px">PUT_for_LocalUser</td>
    <td style="padding:15px">{base_path}/{version}/local-user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForLocalUser(localUserId, callback)</td>
    <td style="padding:15px">DELETE_for_LocalUser</td>
    <td style="padding:15px">{base_path}/{version}/local-user/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForLocalUserByUserId(userId, callback)</td>
    <td style="padding:15px">GET_for_LocalUser_by_user_id</td>
    <td style="padding:15px">{base_path}/{version}/local-user/user-id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForLocalUserByUserId(userId, body, callback)</td>
    <td style="padding:15px">PATCH_for_LocalUser_by_user_id</td>
    <td style="padding:15px">{base_path}/{version}/local-user/user-id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForLocalUserByUserId(userId, body, callback)</td>
    <td style="padding:15px">PUT_for_LocalUser_by_user_id</td>
    <td style="padding:15px">{base_path}/{version}/local-user/user-id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForLocalUserByUserId(userId, callback)</td>
    <td style="padding:15px">DELETE_for_LocalUser_by_user_id</td>
    <td style="padding:15px">{base_path}/{version}/local-user/user-id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForEndpoint(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForEndpoint(body, callback)</td>
    <td style="padding:15px">POST_for_Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForEndpoint(endpointId, callback)</td>
    <td style="padding:15px">GET_for_Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForEndpoint(endpointId, body, callback)</td>
    <td style="padding:15px">PATCH_for_Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForEndpoint(endpointId, body, callback)</td>
    <td style="padding:15px">PUT_for_Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForEndpoint(endpointId, callback)</td>
    <td style="padding:15px">DELETE_for_Endpoint</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForEndpointByMacAddress(macAddress, callback)</td>
    <td style="padding:15px">GET_for_Endpoint_by_mac_address</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/mac-address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForEndpointByMacAddress(macAddress, body, callback)</td>
    <td style="padding:15px">PATCH_for_Endpoint_by_mac_address</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/mac-address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForEndpointByMacAddress(macAddress, body, callback)</td>
    <td style="padding:15px">PUT_for_Endpoint_by_mac_address</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/mac-address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForEndpointByMacAddress(macAddress, callback)</td>
    <td style="padding:15px">DELETE_for_Endpoint_by_mac_address</td>
    <td style="padding:15px">{base_path}/{version}/endpoint/mac-address/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForProxyTarget(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_ProxyTarget</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForProxyTarget(body, callback)</td>
    <td style="padding:15px">POST_for_ProxyTarget</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForProxyTarget(proxyTargetId, callback)</td>
    <td style="padding:15px">GET_for_ProxyTarget</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForProxyTarget(proxyTargetId, body, callback)</td>
    <td style="padding:15px">PATCH_for_ProxyTarget</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForProxyTarget(proxyTargetId, body, callback)</td>
    <td style="padding:15px">PUT_for_ProxyTarget</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForProxyTarget(proxyTargetId, callback)</td>
    <td style="padding:15px">DELETE_for_ProxyTarget</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForProxyTargetByName(name, callback)</td>
    <td style="padding:15px">GET_for_ProxyTarget_by_name</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForProxyTargetByName(name, body, callback)</td>
    <td style="padding:15px">PATCH_for_ProxyTarget_by_name</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForProxyTargetByName(name, body, callback)</td>
    <td style="padding:15px">PUT_for_ProxyTarget_by_name</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForProxyTargetByName(name, callback)</td>
    <td style="padding:15px">DELETE_for_ProxyTarget_by_name</td>
    <td style="padding:15px">{base_path}/{version}/proxy-target/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForNetworkDevice(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_NetworkDevice</td>
    <td style="padding:15px">{base_path}/{version}/network-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForNetworkDevice(body, callback)</td>
    <td style="padding:15px">POST_for_NetworkDevice</td>
    <td style="padding:15px">{base_path}/{version}/network-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForNetworkDevice(networkDeviceId, callback)</td>
    <td style="padding:15px">GET_for_NetworkDevice</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForNetworkDevice(networkDeviceId, body, callback)</td>
    <td style="padding:15px">PATCH_for_NetworkDevice</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForNetworkDevice(networkDeviceId, body, callback)</td>
    <td style="padding:15px">PUT_for_NetworkDevice</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForNetworkDevice(networkDeviceId, callback)</td>
    <td style="padding:15px">DELETE_for_NetworkDevice</td>
    <td style="padding:15px">{base_path}/{version}/network-device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForNetworkDeviceByName(name, callback)</td>
    <td style="padding:15px">GET_for_NetworkDevice_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForNetworkDeviceByName(name, body, callback)</td>
    <td style="padding:15px">PATCH_for_NetworkDevice_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForNetworkDeviceByName(name, body, callback)</td>
    <td style="padding:15px">PUT_for_NetworkDevice_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForNetworkDeviceByName(name, callback)</td>
    <td style="padding:15px">DELETE_for_NetworkDevice_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForNetworkDeviceGroup(filter, sort, offset, limit, calculateCount, callback)</td>
    <td style="padding:15px">GET_for_NetworkDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postForNetworkDeviceGroup(body, callback)</td>
    <td style="padding:15px">POST_for_NetworkDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGETForNetworkDeviceGroup(networkDeviceGroupId, callback)</td>
    <td style="padding:15px">GET_for_NetworkDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForNetworkDeviceGroup(networkDeviceGroupId, body, callback)</td>
    <td style="padding:15px">PATCH_for_NetworkDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForNetworkDeviceGroup(networkDeviceGroupId, body, callback)</td>
    <td style="padding:15px">PUT_for_NetworkDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForNetworkDeviceGroup(networkDeviceGroupId, callback)</td>
    <td style="padding:15px">DELETE_for_NetworkDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForNetworkDeviceGroupByName(name, callback)</td>
    <td style="padding:15px">GET_for_NetworkDeviceGroup_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchForNetworkDeviceGroupByName(name, body, callback)</td>
    <td style="padding:15px">PATCH_for_NetworkDeviceGroup_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putForNetworkDeviceGroupByName(name, body, callback)</td>
    <td style="padding:15px">PUT_for_NetworkDeviceGroup_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForNetworkDeviceGroupByName(name, callback)</td>
    <td style="padding:15px">DELETE_for_NetworkDeviceGroup_by_name</td>
    <td style="padding:15px">{base_path}/{version}/network-device-group/name/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
